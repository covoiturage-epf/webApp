import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Accueil', icon: 'ni-tv-2 text-danger', class: '' },
  { path: '/offer-a-carpool', title: 'Proposer une course', icon: 'ni-book-bookmark text-danger', class: '' },
  { path: '/create-vehicle', title: 'Ajouter un vehicule', icon: 'ni ni-bus-front-12 text-primary', class: '' },

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[] | undefined;
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }
}
