import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-licence-plate',
  templateUrl: './licence-plate.component.html',
  styleUrls: ['./licence-plate.component.css']
})
export class LicencePlateComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  id_user!: any;
  constructor(public activeModal: NgbActiveModal) {
    this.title = "";
    this.message = "";
    this.id_user = localStorage.getItem('id_user');
  }

  ngOnInit(): void {
  }

}
