import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/booking/booking';
import { BookingserviceService } from 'src/app/service/booking-service/bookingservice.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-nbplaces',
  templateUrl: './nbplaces.component.html',
  styleUrls: ['./nbplaces.component.css']
})
export class NbplacesComponent implements OnInit {

  booking!: any;
  afficher: boolean = false;
  driverId!: any;
  userId!: any;



  constructor(private bookingService: BookingserviceService,
    public activeModal: NgbActiveModal) {
    this.booking = new Booking('', 0, '', 0, 0);

  }

  ngOnInit(): void {
    this.driverId = localStorage.getItem('driverId');
    this.userId = localStorage.getItem('userId');
    this.booking.accountId = this.userId;
    console.log(this.driverId)
    this.booking.driveId = this.driverId;
  }
  get reservedSeats() {
    return this.booking.seats;
  }
  afficher_modal() {
    this.afficher = true;

  }
  confirmer_modal() {


    console.log(this.booking)
    this.booking.seating
    this.bookingService.create(this.booking).subscribe(result => {
      console.log(result);
    })
    this.activeModal.close()


  }
  fermer_modal() {
    this.afficher = false;
  }
}
