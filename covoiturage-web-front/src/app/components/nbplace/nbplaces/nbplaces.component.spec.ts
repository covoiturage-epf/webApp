import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NbplacesComponent } from './nbplaces.component';

describe('NbplacesComponent', () => {
  let component: NbplacesComponent;
  let fixture: ComponentFixture<NbplacesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NbplacesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NbplacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
