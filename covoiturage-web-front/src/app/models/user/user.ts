export class User {
    id: string;
    name: string;
    firstName: string;
    email: string;
    phone: string;
    password: string;
    role: string;
    constructor(
        id: string,
        name: string,
        firstName: string,
        email: string,
        phone: string,
        password: string,
        role: string,) {

        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role = role;
    }
}
