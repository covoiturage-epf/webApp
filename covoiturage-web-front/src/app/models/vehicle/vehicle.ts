export class Vehicle {

    manufacturer: string;
    model: string;
    number_of_seats: string;
    color: string;
    licence_plate: string;
    user_email: string;
    constructor(

        manufacturer: string,
        model: string,
        number_of_seats: string,
        color: string,
        licence_plate: string,
        user_email: string,) {

        this.manufacturer = manufacturer;
        this.model = model;
        this.number_of_seats = number_of_seats;
        this.color = color;
        this.licence_plate = licence_plate;
        this.user_email = user_email;
    }
}
