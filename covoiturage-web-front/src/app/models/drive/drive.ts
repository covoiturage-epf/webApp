export class Drive {
    id: string;
    pickup: string;
    destination: string;
    deptime: string;
    price: string;
    seating: number;
    details: string;
    //user_email: string;
    //driver_id:number;
    driverId: number;
    // participants: any[];
    constructor(

        id: string,
        pickup: string,
        destination: string,
        deptime: string,
        price: string,
        seating: number,
        details: string,
        //user_email: string,
        //participants: any[],
        //driver_id:number,
        driverId: number) {

        this.id = id;
        this.pickup = pickup;
        this.destination = destination;
        this.deptime = deptime;
        this.price = price;
        this.seating = seating;
        this.details = details;
        //this.user_email = user_email;
        //this.driver_id = driver_id;
        this.driverId = driverId;
        //this.participants = participants;
    }
}
