export class Booking {
    id: string;
    seats: number;
    status: string;
    accountId: number;
    driveId: number;

    constructor(
        id: string,
        seats: number,
        status: string,
        accountId: number,
        driveId: number
    ) {
        this.id = id;
        this.seats = seats;
        this.status = status;
        this.accountId = accountId;
        this.driveId = driveId;

    }
}
