import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/booking/booking';
import { Drive } from 'src/app/models/drive/drive';
import { BookingserviceService } from 'src/app/service/booking-service/bookingservice.service';
import { DriveserviceService } from 'src/app/service/drive-service/driveservice.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LicencePlateComponent } from 'src/app/components/licenceplate_modal/licence-plate.component';
import { NbplacesComponent } from 'src/app/components/nbplace/nbplaces/nbplaces.component';
import { User } from 'src/app/models/user/user';
import { UserserviceService } from 'src/app/service/user-service/userservice.service';
import { Observable, catchError, map, of } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  drives!: any[];
  bookings!: any[];
  bookingsByDrivesId!: any[]
  email!: any;
  id_user!: any;
  id_drive!: any;
  drive: Drive;
  booking!: any;
  userFound: User;
  afficher: boolean = false;

  userNames: { [key: number]: string } = {};


  constructor(
    private userService: UserserviceService,
    private driveService: DriveserviceService,
    private bookingService: BookingserviceService,
    private modalService: NgbModal
  ) {


    this.email = localStorage.getItem('useremail');
    this.id_user = localStorage.getItem('id_user');
    this.drive = new Drive('', '', '', '', '', 0, '', 0);
    this.booking = new Booking('', 0, '', 0, 0);
    this.userFound = new User('', '', '', '', '', '', '');
    console.log(this.drive);
    console.log(this.email)


    // this.driveService.getDrives().subscribe(data => {
    //   this.drives = data;
    // for (let index = 0; index < this.drives.length; index++) {
    //   const element = this.drives[index];
    //   for (let index = 0; index < element.participants.length; index++) {
    //     const participant = element.participants[index];
    //     if (participant == this.email) {
    //       this.verif = true;
    //     }
    //   }


    //   console.log(element.participants);

    // }
    //   console.log(this.drives);
    // })
  }

  ngOnInit(): void {
    this.driveService.getDrives().subscribe(data => {
      this.drives = data;
    })
    this.bookingService.getBookingsByAccountId(this.id_user).subscribe(data => {
      this.bookings = data;
      console.log(this.bookings);
      console.log(typeof (this.bookings[1].reservationDate));

    })



    this.bookingService.getBookingsByDriverId(this.id_user).subscribe(data => {
      this.bookingsByDrivesId = data;
      console.log(this.bookingsByDrivesId);
      console.log(this.bookingsByDrivesId[1].accountId);

      this.bookingsByDrivesId.forEach((reservation) => {
        this.findUserById(reservation.accountId).subscribe((userName: string) => {
          this.userNames[reservation.accountId] = userName;
        });
      });
    });
    // this.bookingService.getBookingsByDriveId(this.id_drive).subscribe(data => {
    //   this.bookingsByDrivesId = data
    // })

    //this.findUserById(this.id_user)

  }

  get reservedSeats() {
    return this.booking.seats;
  }



  participer_drive(drive: Drive) {
    this.booking.accountId = this.id_user;
    this.booking.driveId = drive.id;

    localStorage.setItem('userId', this.id_user);
    localStorage.setItem('driverId', drive.id);

    console.log(drive.id)
    this.modalService.open(NbplacesComponent);
  }


  formatDate(dateString: string) {
    const date = new Date(dateString);
    const formattedDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
    const formattedTime = `${date.getHours()}:${date.getMinutes()}`;

    return (formattedDate + " à " + formattedTime);
  }

  formatDate1(dateString: string) {
    const date = new Date(dateString);
    const day = date.getDate();
    const month = date.getMonth() + 1; // Les mois sont indexés à partir de 0, donc on ajoute 1
    const year = date.getFullYear();
    const hours = date.getHours();
    const minutes = date.getMinutes();

    // Ajout d'un zéro devant le jour et le mois si nécessaire pour avoir deux chiffres
    const formattedDay = (day < 10) ? `0${day}` : day;
    const formattedMonth = (month < 10) ? `0${month}` : month;
    const formattedHours = (hours < 10) ? `0${hours}` : hours;
    const formattedMinutes = (minutes < 10) ? `0${minutes}` : minutes;
    //const formattedDate = `${formattedDay}/${formattedMonth}/${year}`;

    const formattedDateTime = `${formattedDay}/${formattedMonth}/${year} ${formattedHours}:${formattedMinutes}`;

    return formattedDateTime;

  }


  // findUserByIdyy(accountId: string) {
  //   this.userService.findById(parseInt(accountId))
  //     .subscribe(
  //       (userfound: User) => {
  //         this.userFound = userfound
  //         console.log('Compte récupéré avec succès:', this.userFound);
  //         console.log(this.userFound.name + " " + this.userFound.firstName);
  //         return this.userFound.name
  //       },
  //       (error: any) => {
  //         console.error('Erreur lors de la récupération du compte:', error);
  //       }
  //     );
  // }


  findUserById(accountId: string): Observable<string> {
    return this.userService.findById(parseInt(accountId)).pipe(
      map((userfound: User) => {
        console.log('Compte récupéré avec succès:', userfound);
        console.log(userfound.name + " " + userfound.firstName);
        return (userfound.name + " " + userfound.firstName);
      }),
      catchError((error: any) => {
        console.error('Erreur lors de la récupération du compte:', error);
        return of('Erreur de récupération du nom');
      })
    );
  }

  // this.bookings.forEach(booking => {
  //   this.findByIdObservable[booking.accountId] = this.userService.findById(booking.accountId);
  //   this.findByIdError[booking.accountId] = this.findByIdObservable[booking.accountId].pipe(
  //     catchError(error => of(error.message))
  //   );
  // });

  acceptBooking(booking: Booking) {

    this.bookingService.update(parseInt(booking.id), 1);
    console.log(booking.id)
  }
  refuseBooking(booking: Booking) {
    this.bookingService.update(parseInt(booking.id), 2);
  }

  closeBooking(booking: Booking) {
    this.bookingService.update(parseInt(booking.id), 4);
  }


}
