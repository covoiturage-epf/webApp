import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user/user';
import { Vehicle } from 'src/app/models/vehicle/vehicle';
import { UserserviceService } from 'src/app/service/user-service/userservice.service';
import { VehicleserviceService } from 'src/app/service/vehicle-service/vehicleservice.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LicencePlateComponent } from 'src/app/components/licenceplate_modal/licence-plate.component';


@Component({
  selector: 'app-create-vehicle',
  templateUrl: './create-vehicle.component.html',
  styleUrls: ['./create-vehicle.component.css']
})
export class CreateVehicleComponent {

  vehicle: Vehicle;
  userData: User;
  email!: any;


  constructor(

    private router: Router,
    private vehicleService: VehicleserviceService,
    private userService: UserserviceService,
    private modalService: NgbModal
  ) {
    this.vehicle = new Vehicle('', '', '', '', '', '');

    this.userData = new User('', '', '', '', '', '', '');
    this.email = localStorage.getItem('useremail')?.toString;

    console.log(localStorage.getItem('useremail')?.toString())
    this.vehicle.user_email = this.email;
    // this.userService.dataUpdated.subscribe((user: User) => {
    //   this.userData = user
    //   console.log(this.userData);
    // })


  }


  get manufacturer() {

    return this.vehicle.manufacturer;
  }
  get model() {

    return this.vehicle.model;
  }
  get number_of_seats() {

    return this.vehicle.number_of_seats;
  }
  get color() {

    return this.vehicle.color;
  }
  get licence_plate() {

    return this.vehicle.licence_plate;
  }
  get user_account() {
    this.vehicle.user_email = this.email;

    return this.vehicle.user_email;
  }

  onSubmit() {

    this.vehicleService.create(this.vehicle).subscribe(result => {

      this.gotohome();
      // if (this.userData.role === "0") {
      //   console.log(result);

      //   const modalRef = this.modalService.open(LicencePlateComponent);
      //   modalRef.componentInstance.title = 'Attention';
      //   modalRef.componentInstance.message = 'Veuillez ajouter votre permis.';

      //   this.stay();
      // } else {
      //   console.log(result);
      //   this.gotohome();
      // }
    })
  }
  gotohome() {
    this.router.navigate(['/dashboard']);
  }
  stay() {
    window.location.reload();
  }
}
