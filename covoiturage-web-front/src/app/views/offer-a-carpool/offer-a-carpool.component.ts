import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Drive } from 'src/app/models/drive/drive';
import { Vehicle } from 'src/app/models/vehicle/vehicle';
import { User } from 'src/app/models/user/user';
import { DriveserviceService } from 'src/app/service/drive-service/driveservice.service';
import { VehicleserviceService } from 'src/app/service/vehicle-service/vehicleservice.service';
import { UserserviceService } from 'src/app/service/user-service/userservice.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LicencePlateComponent } from 'src/app/components/licenceplate_modal/licence-plate.component';
import { GoogleMapsServiceService } from 'src/app/service/googleMapService/google-maps-service.service';

@Component({
  selector: 'app-offer-a-carpool',
  templateUrl: './offer-a-carpool.component.html',
  styleUrls: ['./offer-a-carpool.component.css']
})
export class OfferACarpoolComponent {

  drive: Drive;
  //userData: User;
  //user: User;

  user_email!: any;
  user_pwd!: any;
  user_role!: any;
  user_id!: any;

  searchQuery = '';
  suggestions: any[] = [];



  constructor(

    private router: Router,
    private vehicleService: VehicleserviceService,
    private driveService: DriveserviceService,
    private userService: UserserviceService,
    private modalService: NgbModal,
    private googleMapsService: GoogleMapsServiceService
  ) {
    this.drive = new Drive('', '', '', '', '', 0, '', 0);
    console.log(this.drive);

    //this.user = new User('', '', '', '', '', '', '');


    this.user_email = localStorage.getItem('useremail');
    this.user_pwd = localStorage.getItem('userpwd');
    this.user_role = localStorage.getItem('user_role');
    this.user_id = localStorage.getItem('id_user');
    //this.userData = new User('', '', '', '', '', '', '');

    // this.user.email = this.email;
    // this.user.password = this.pwd;
    // this.user.role = this.role;

    console.log(this.user_role)
    // console.log(this.user.role === "false")
    // this.userService.login(this.user).subscribe((user: User) => {
    //   this.userData = user
    //   console.log(this.userData);
    // })

  }


  get pickup() {

    return this.drive.pickup;
  }
  get destination() {

    return this.drive.destination;
  }
  get deptime() {

    return this.drive.deptime;
  }
  get price() {

    return this.drive.price;
  }
  get seating() {

    return this.drive.seating;
  }
  get details() {

    return this.drive.details;
  }
  // get userEmail() {
  //   return this.drive.user_email;
  // }



  onSubmit() {
    console.log(this.drive);


    if (this.user_role === "false") {

      this.modalService.open(LicencePlateComponent);

    } else {
      //this.drive.user_email = this.user.email;
      this.drive.driverId = this.user_id;
      this.driveService.create(this.drive).subscribe(result => {
        console.log(result);
        this.gotohome();
      })
    }

    // this.driveService.create(this.drive).subscribe(result => {

    //   if (this.user.role === "false") {
    //     console.log(result);
    //     this.stay();
    //     console.log(this.userData);
    //     const modalRef = this.modalService.open(LicencePlateComponent);
    //     modalRef.componentInstance.title = 'Attention';
    //     modalRef.componentInstance.message = 'Veuillez ajouter votre permis.';
    //   } else {
    //     console.log(result);
    //     this.gotohome();
    //   }
    // })
  }
  gotohome() {
    this.router.navigate(['/dashboard']);
  }
  stay() {
    window.location.reload();
  }

  // searchCities(addresse: string) {
  //   this.googlemapService.getCityPredictions(addresse)
  //     .then((predictions: string[]) => {
  //       this.cityPredictions = predictions;
  //     })
  //     .catch((error: any) => {
  //       console.error('Une erreur s\'est produite lors de la récupération des prédictions de ville:', error);
  //       this.cityPredictions = [];
  //     });
  // }

  searchAutocomplete(input: string) {
    if (input.length >= 3) {
      this.googleMapsService.autocompletePlace(input)
        .subscribe((response: any) => {
          this.suggestions = response.predictions.map((prediction: any) => prediction.description);
          console.log(response)
          console.log(this.suggestions)
        }, (error: any) => {
          console.error('Erreur lors de la récupération des suggestions d\'autocomplétion :', error);
        });
    } else {
      this.suggestions = [];
    }
  }
}
