import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferACarpoolComponent } from './offer-a-carpool.component';

describe('OfferACarpoolComponent', () => {
  let component: OfferACarpoolComponent;
  let fixture: ComponentFixture<OfferACarpoolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferACarpoolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferACarpoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
