import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user/user';
import { UserserviceService } from 'src/app/service/user-service/userservice.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  user: User;
  userData: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserserviceService
  ) {
    this.user = new User('', '', '', '', '', '', '');
    console.log(this.user);
    this.userData = new User('', '', '', '', '', '', '');

  }


  get email() {

    return this.user.email;
  }
  get password() {

    return this.user.password;
  }
  // sendData(){
  //   this.userService.updateData(this.userData);
  // }


  onSubmit() {
    console.log(this.user);

    this.userService.login(this.user).subscribe(result => {

      if (result.email === null) {
        console.log(result);
        this.stay();
      } else {
        console.log(result);
        console.log(result.role)
        //this.userService.updateData(this.userData);
        localStorage.setItem('id_user', result.id);
        localStorage.setItem('useremail', result.email);
        localStorage.setItem('userpwd', JSON.stringify(result.password));
        localStorage.setItem('user_role', JSON.stringify(result.role))
        this.gotohome();
        this.userData = result;

      }
    })


  }
  gotohome() {
    this.router.navigate(['/dashboard']);
  }
  stay() {
    window.location.reload();
  }
}
