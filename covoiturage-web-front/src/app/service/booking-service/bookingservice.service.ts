import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Booking } from 'src/app/models/booking/booking';

@Injectable({
  providedIn: 'root'
})
export class BookingserviceService {
  private bookingUrl: string;
  constructor(private http: HttpClient) {
    this.bookingUrl = 'http://localhost:8080/api/booking/';

  }
  public create(booking: Booking) {
    return this.http.post<Booking>(this.bookingUrl, booking);
  }

  public update(idBooking: number, statusValue: number) {
    const val = 0;
    const params = {
      id_Booking: idBooking,
      Status_value: statusValue,
      note_booking: val

    };
    console.log(params.id_Booking)
    this.http.post(this.bookingUrl + "update", null, { params }).subscribe(() => {
      console.log('La réservation a été mise à jour avec succès.');
    },
      (error) => {
        console.error('Erreur lors de la mise à jour de la réservation :', error);
      });
  }

  updateBooking(booking: Booking) {
    return this.http.post(this.bookingUrl + 'update', booking);
  }



  public getBookingsByDriveId(id_Drive: number) {
    return this.http.get<Booking[]>(this.bookingUrl + `DrivesBookings/${id_Drive}`);
  }

  public getBookingsByAccountId(id_Account: number) {
    return this.http.get<Booking[]>(this.bookingUrl + `myPassengerBookings/${id_Account}`);
  }


  public getBookingsByDriverId(id_Driver: number) {
    return this.http.get<Booking[]>(this.bookingUrl + `myDriverBookings/${id_Driver}`);
  }
}
