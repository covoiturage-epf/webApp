import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapsServiceService {

  private apiKey: string = 'AIzaSyC_0t1JWLLMAuKg5aygvTZo6xj9cUIYfXc';
  constructor(private http: HttpClient) {
  }




  loadScript() {
    return new Promise<void>((resolve, reject) => {
      const script = document.createElement('script');
      script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC_0t1JWLLMAuKg5aygvTZo6xj9cUIYfXc&callback=googleMapsLoaded';
      script.onload = () => {
        resolve();
      };
      script.onerror = () => {
        reject();
      };
      document.body.appendChild(script);
    });
  }


  getCityAddresses(city: string) {
    // URL de l'API Google Maps pour récupérer les adresses de ville
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${city}&key=AIzaSyC_0t1JWLLMAuKg5aygvTZo6xj9cUIYfXc`;


    // Effectuer la requête HTTP GET
    return this.http.get(url);
  }

  autocompletePlace(input: string): Observable<any> {
    const url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
    const params = new HttpParams()
      .set('input', input)
      .set('key', this.apiKey);
    return this.http.get(url, { params });

    //"start": "ng serve --proxy-config proxy.conf.json"

  }

}
