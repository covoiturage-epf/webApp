import { EventEmitter, Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/models/user/user';
import { Observable } from 'rxjs';

@Injectable()
export class UserserviceService {
  private usersUrl: string;
  dataUpdated = new EventEmitter<User>();


  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8080/api/authentication';
  }


  public login(user: User) {
    return this.http.post<User>(this.usersUrl, user);
  }
  public updateData(user: User) {
    this.dataUpdated.emit(user)
  }

  public findById(id: Number) {
    const uri = `http://localhost:8080/api/account/findById/${id}`;
    return this.http.get<User>(uri);
  }

}