import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vehicle } from 'src/app/models/vehicle/vehicle';

@Injectable()
export class VehicleserviceService {
  private vehicleUrl: string;

  constructor(private http: HttpClient) {
    this.vehicleUrl = 'http://localhost:8080/api/vehicle/';
  }

  public create(vehicle: Vehicle) {
    return this.http.post<Vehicle>(this.vehicleUrl + "saveVehicle", vehicle);
  }


}
