import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Drive } from 'src/app/models/drive/drive';
import { User } from 'src/app/models/user/user';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DriveserviceService {
  private driveUrl: string;
  constructor(private http: HttpClient) {
    this.driveUrl = 'http://localhost:8080/api/drive/';

  }

  public create(drive: Drive) {
    return this.http.post<Drive>(this.driveUrl, drive);
  }

  public getDriverByClientId(drive: Drive) {
    return this.http.get<Drive>(this.driveUrl + "findDriveByAccountId/${drive.id}");
  }

  public getDrives(): Observable<Drive[]> {
    return this.http.get<Drive[]>(this.driveUrl);
  }

  public update(drive: Drive) {
    return this.http.post<Drive>(this.driveUrl + "editDrive", drive);
  }
}
