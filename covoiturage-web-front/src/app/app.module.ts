import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { ConnectedLayoutComponent } from './layouts/connected-layout/connected-layout.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from './components/components.module';


import { UserserviceService } from './service/user-service/userservice.service';
import { VehicleserviceService } from './service/vehicle-service/vehicleservice.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    ConnectedLayoutComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ComponentsModule
  ],
  providers: [
    UserserviceService,
    VehicleserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
