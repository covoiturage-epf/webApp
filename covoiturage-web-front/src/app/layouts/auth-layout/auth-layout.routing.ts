import { Routes } from '@angular/router';

import { LoginComponent } from '../../views/login/login.component';

export const AuthLayoutRoutes: Routes = [
    { path: 'login', component: LoginComponent },
];
