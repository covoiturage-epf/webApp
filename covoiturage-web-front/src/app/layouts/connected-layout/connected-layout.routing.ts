import { Routes } from '@angular/router';
import { OfferACarpoolComponent } from 'src/app/views/offer-a-carpool/offer-a-carpool.component';
import { CreateVehicleComponent } from 'src/app/views/create-vehicle/create-vehicle.component';

import { DashboardComponent } from '../../views/dashboard/dashboard.component';

export const ConnectedLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'offer-a-carpool', component: OfferACarpoolComponent },
    { path: 'create-vehicle', component: CreateVehicleComponent }


];
