import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ConnectedLayoutRoutes } from './connected-layout.routing';
import { DashboardComponent } from 'src/app/views/dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { OfferACarpoolComponent } from 'src/app/views/offer-a-carpool/offer-a-carpool.component';
import { CreateVehicleComponent } from 'src/app/views/create-vehicle/create-vehicle.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ConnectedLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule
  ],
  declarations: [
    DashboardComponent,
    OfferACarpoolComponent,
    CreateVehicleComponent
  ]

})
export class ConnectedLayoutModule { }
